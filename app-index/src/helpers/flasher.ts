import JSZip from "jszip"
import JSZipUtils from "jszip-utils"
import { fromUint8Array } from "js-base64"

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms))

class SerialConnector {
    port: SerialPort
    encoder: TextEncoder
    decoder: TextDecoder
    reader: ReadableStreamDefaultReader | undefined
    writer: WritableStreamDefaultWriter | undefined

    constructor(port: SerialPort) {
        this.port = port
        this.encoder = new TextEncoder()
        this.decoder = new TextDecoder()
    }

    async open() {
        await this.port.open({ baudRate: 115200 })
        this.reader = this.port.readable.getReader({ type: "bytes" })
        this.writer = this.port.writable.getWriter({ type: "bytes" })
        await this.write("\r\x03\x03")
        await this.readUntil(">")
        await this.write("\r\x01")
        await this.readUntil(">")
    }

    async write(text: string) {
        if (!this.writer) {
            return
        }
        const result = await this.writer.write(this.encoder.encode(text))
        await sleep(10)
        return result
    }

    async read(expect: string | undefined) {
        if (!this.reader) {
            return
        }
        const result = await this.reader.read()
        let text = this.decoder.decode(result.value)
        if (expect) {
            if (text.startsWith(expect)) {
                text = text.substring(expect.length, text.length)
            } else {
                throw "Unexpected read"
            }
        }
        return text
    }

    async readUntil(stop: string) {
        if (!this.reader) {
            return
        }
        while (true) {
            let data = (await this.read(undefined))?.trim()
            if (data?.endsWith(stop)) break
        }
    }

    async exec(cmd: String) {
        await this.write(cmd + "\x04")
        await this.read("OK\x04\x04>")
    }

    async writeBlob(path: String, blob: Uint8Array) {
        const dir = JSON.stringify(path.substring(0, path.lastIndexOf("/")))
        const result = await this.exec(
            `from binascii import a2b_base64 as b64d; import os;\nif not os.path.exists(${dir}):\n  os.mkdir(${dir})`
        )
        await this.exec(`f = open(${JSON.stringify(path)}, 'w');`)
        for (let i = 0; i < blob.length; i += 150) {
            let slice = blob.slice(i, i + 150)
            await this.exec(`f.write(b64d(b"${fromUint8Array(slice)}"))`)
            await sleep(10)
        }
        await this.exec("f.close()")
    }
}

export async function flash(url: string, onerror: Function) {
    try {
        const usbVendorId = 0x303a
        const port = await navigator.serial.requestPort({
            filters: [{ usbVendorId }],
        })
        const serial = new SerialConnector(port)
        await serial.open()
        const zip = await JSZipUtils.getBinaryContent(url)
        const data = await JSZip.loadAsync(zip)
        let files: Record<string, Promise<Uint8Array>> = {}
        data.forEach(async (path, file) => {
            files[path] = file.async("uint8array")
        })
        for (let path in files) {
            await serial.writeBlob("/flash/sys/apps/" + path, await files[path])
        }
        await serial.write("\r\x02\r\x04")
        await port.forget()
        alert("Done!")
    } catch (e) {
        console.error(e)
        onerror((e as Error).message || "Something went wrong, try again!")
    }
}
