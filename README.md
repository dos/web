# flow3r.garden website and web tools

## Website

Built using hugo and blowfish theme
Blowfish docs: https://blowfish.page/docs/
Hugo install guide: https://gohugo.io/installation/

If you want to create a new blog post or guide, create a new folder in `content/posts` (blog) or `content/guides` and create an `_index.md` in that folder. See previous entries as a reference.
To change the landing page edit the `_index.md` in the `content` root folder.
Most stuff is configured in the toml files located in `config/_default`

Run `hugo` to build the site or `hugo server` to run a local dev server that auto reloads on every change.

## How to setup Hugo dev server?

1. Install Hugo
2. Init submodules: `git submodule update --init --recursive`
3. Run Hugo dev server: `hugo server`

## Web Flasher

flow3r Web flasher tool. See `webflasher/README.md`.

## App List

flow3r App list. See `app-index/README.md`.
