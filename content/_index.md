## [Get more apps](https://flow3r.garden/apps)

> Seeing a dev / v0 version when starting your badge? Give your flow3r some love and update its firmware. To do so, try out our [web flasher](https://flow3r.garden/flasher) (Chrome only) or follow the README inside the latest [release](https://git.flow3r.garden/flow3r/flow3r-firmware/-/releases) (the file listed under "Other").

## How to grow your flow3r

<iframe style="aspect-ratio: 16 / 9; width: 100%; height: auto;" width="655" height="368" src="https://media.ccc.de/v/camp2023-101-the-flow3r-badge-assembly-i/oembed" frameborder="0" allowfullscreen></iframe>

Also check out our [detailed instructions](https://flow3r.garden/assembly).

### Find new apps and distribute your own apps

Use [this website](https://flow3r.garden/apps) to find new apps for your badge. Make you apps show up on the website using [this guide](https://git.flow3r.garden/flow3r/flow3r-apps).

### Documentation & Guides

Browse [our documentation](https://docs.flow3r.garden/) to get familiar with what your badge has to offer.

### Learn to code

[This guide](https://docs.flow3r.garden/badge/programming.html) shows you how to get started with writing Micropython on the flow3r.
